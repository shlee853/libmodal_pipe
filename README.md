# libmodal_pipe

This is a library to support named-pipe communication between a local server and multiple local clients. This is used for the voxl-imu-server.

To test and demonstrate its function, this includes an example "modal-hello-server" and "modal-hello-client" that can be tried for fun. The server simple sends "hello" to the client once a second. This can be used to test the pipe behavior of multiple clients starting and stopping.


## Build Instructions

1) prerequisite: your own GCC of choice or the voxl-cross docker image

Follow the instructions here to build and install the voxl-cross docker image:

https://gitlab.com/voxl-public/voxl-docker


2) Launch the voxl-cross docker and make sure this project directory is mounted inside the Docker.

```bash
~/git/libmodal_json$ voxl-docker -i voxl-cross
root@silverstone:/home/user#
root@silverstone:/home/user# ls
CHANGELOG       LICENSE    build.sh  conf     install_on_voxl.sh  library          readers
CMakeLists.txt  README.md  clean.sh  example  ipk                 make_package.sh
root@silverstone:/home/user#
```

3) The build script takes one of these 4 options to set the architecture this should build for:

* `./build.sh native` Builds using whatever native gcc lives at /usr/bin/gcc
* `./build.sh 32`     Builds using the 32-bit (armv7) arm-linux-gnueabi cross-compiler that's in the voxl-cross docker
* `./build.sh 64`     Builds using the 64-bit (armv8) aarch64-gnu cross-compiler that's in the voxl-cross docker
* `./build.sh both`   Builds both 32 and 64 cross-compiled binaries. This is what's used to build the VOXL IPK packages

```bash
bash-4.3$ ./build.sh both
```

4) Make an ipk package while still inside the docker.

```bash
~/git/libmodal_json$ ./make_package.sh

Package Name:  libmodal_pipe
version Number:  x.x.x
ar: creating libmodal_pipe_x.x.x.ipk

DONE
```

This will make a new libmodal_pipe_x.x.x.ipk file in your working directory. The name and version number came from the ipk/control/control file. If you are updating the package version, edit it there.

It will install whatever was built in the previous step based on your chosen compiler. For the "both" option it will install 32 and 64 bit versions of the lib, one copy of the headers, and just the 64-bit example programs.


## Deploy to VOXL

You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this OUTSIDE of docker as your docker image probably doesn't have usb permissions for ADB.

```bash
~/git/libmodal_pipe$ ./install_on_voxl.sh
pushing libmodal_pipe_0.0.1_8x96.ipk to target
searching for ADB device
adb device found
libmodal_pipe_1.1.0.ipk: 1 file pushed. 2.1 MB/s (51392 bytes in 0.023s)
Removing package libmodal_pipe from root...
Installing libmodal_pipe (0.0.1) on root.
Configuring libmodal_pipe

Done installing libmodal_pipe
```

