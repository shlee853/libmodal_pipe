/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <modal_start_stop.h>
#include <modal_pipe_server.h>

#define PROCESS_NAME	"modal-hello-server" // name of pid file

// this is the directory used by the voxl-hello-server for named pipes
#define PIPE_DIR	(MODAL_PIPE_DEFAULT_BASE_DIR "hello/")

// local vars
static int en_debug;


// printed if some invalid argument was given
static void print_usage(void)
{
	printf("\n\
modal-hello-server usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, modal-hello-server will automatically\n\
stop the background service so you don't have to stop it manually\n\
\n\
-d, --debug                 print debug info\n\
-h, --help                  print this help message\n\
\n");
	return;
}


static void control_pipe_handler(int ch, char* string, int bytes, __attribute__((unused)) void* context)
{
	printf("received command on channel %d bytes: %d string: \"%s\"\n", ch, bytes, string);
	return;
}


static void request_pipe_handler(int ch, char* string, __attribute__((unused)) int bytes, int client_id, __attribute__((unused)) void* context)
{
	printf("client \"%s\" connected to channel %d  with client id %d\n", string, ch, client_id);
	return;
}


static void disconnect_handler(int ch, int client_id, char* name, __attribute__((unused)) void* context)
{
	printf("client \"%s\" with id %d has disconnected from channel %d\n", name, client_id, ch);
	return;
}


static int __parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",				no_argument,	0,	'd'},
		{"help",				no_argument,	0,	'h'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "dh", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'd':
			en_debug = 1;
			break;

		case 'h':
			print_usage();
			return -1;

		default:
			print_usage();
			return -1;
		}
	}

	return 0;
}



int main(int argc, char* argv[])
{
	// check for options
	if(__parse_opts(argc, argv)) return -1;

////////////////////////////////////////////////////////////////////////////////
// gracefully handle an existing instance of the process and associated PID file
////////////////////////////////////////////////////////////////////////////////

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

////////////////////////////////////////////////////////////////////////////////
// do any setup and start threads HERE
////////////////////////////////////////////////////////////////////////////////

	// enable the control pipe feature
	int flags = SERVER_FLAG_EN_CONTROL_PIPE;
	// enable the info pipe feature
	flags |= SERVER_FLAG_EN_INFO_PIPE;
	if(en_debug) flags|=SERVER_FLAG_EN_DEBUG_PRINTS;

	// start first channel
	if(pipe_server_init_channel(0, PIPE_DIR, flags)) return -1;
	pipe_server_set_control_cb(0, &control_pipe_handler, NULL);
	pipe_server_set_request_cb(0, &request_pipe_handler, NULL);
	pipe_server_set_disconnect_cb(0, &disconnect_handler, NULL);
	pipe_server_set_info_string(0, "This is the info string from modal-hello-server!");

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

////////////////////////////////////////////////////////////////////////////////
// all threads started, wait for signal handler to stop it
////////////////////////////////////////////////////////////////////////////////

	main_running=1; // this is an extern variable in start_stop.c
	char str[32];
	char len;
	int i = 0;

	printf("Init complete, entering main loop\n");
	while(main_running){

		// make new string to send
		len = sprintf(str,"hello%d", i);
		i++;

		// send to first channel
		if(en_debug){
			int n = pipe_server_get_num_clients(0);
			printf("sending \"%s\" to %d connected clients\n", str, n);
		}
		pipe_server_send_to_channel(0, str, len+1);

		// run about 1hz
		usleep(1000000);
	}

////////////////////////////////////////////////////////////////////////////////
// Stop all the threads and do cleanup HERE
////////////////////////////////////////////////////////////////////////////////

	printf("Starting shutdown sequence\n");
	pipe_server_close_all();
	remove_pid_file(PROCESS_NAME);
	printf("exiting cleanly\n");
	return 0;
}
