/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>

#include <modal_start_stop.h>
#include <modal_pipe_sink.h>


// this is the directory used by the voxl-hello-server for named pipes
#define CHANNEL1_PATH	(MODAL_PIPE_DEFAULT_BASE_DIR "hello-sink")
#define CHANNEL2_PATH	(MODAL_PIPE_DEFAULT_BASE_DIR "hello-sink2")

// you may need a larger buffer for your application!
#define PIPE_READ_BUF_SIZE	1024


static void simple_cb(int ch, char* data, int bytes, __attribute__((unused)) void* context){
	// for this tester we expect users to echo to the sink
	// echo does not write a null-terminated string, it's newline terminated,
	// so specify the number of bytes for printf to print or it will print
	// any other nonsense in the buffer up to the first NULL
	// # bytes includes the newline character
	printf("received %d bytes on channel %d: %.*s", bytes, ch, bytes, data);
	return;
}

int main()
{
	// set some basic signal handling for safe shutdown
	enable_signal_handler();

	// start both sinks
	if(pipe_sink_init_channel(0, CHANNEL1_PATH, EN_PIPE_SINK_SIMPLE_HELPER, PIPE_READ_BUF_SIZE)) return -1;
	pipe_sink_set_simple_cb(0, &simple_cb, NULL);

	if(pipe_sink_init_channel(1, CHANNEL2_PATH, EN_PIPE_SINK_SIMPLE_HELPER, PIPE_READ_BUF_SIZE)) return -1;
	pipe_sink_set_simple_cb(1, &simple_cb, NULL);


	printf("done initializing, try the following commands:\n");
	printf("echo hello > %s\n", CHANNEL1_PATH);
	printf("echo hello > %s\n", CHANNEL2_PATH);

	// keep going until signal handler sets the running flag to 0
	main_running = 1;
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	pipe_sink_close_all();

	printf("exiting cleanly\n");
	return 0;
}