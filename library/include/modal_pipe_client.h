/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef MODAL_PIPE_CLIENT_H
#define MODAL_PIPE_CLIENT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <modal_pipe_common.h>
#include <modal_pipe_interfaces.h>

// Sensible limit on the number of servers a client can connect to
#define PIPE_CLIENT_MAX_CHANNELS	16

// error codes returned by pipe_client_init_channel()
#define PIPE_ERROR_OTHER					-1
#define PIPE_ERROR_SERVER_NOT_AVAILABLE		-2
#define PIPE_ERROR_REACHED_MAX_NAME_INDEX	-3
#define PIPE_ERROR_FILE_IO					-4
#define PIPE_ERROR_TIMEOUT					-5
#define PIPE_ERROR_INVALID_ARG				-6
#define PIPE_ERROR_NOT_CONNECTED			-7
#define PIPE_ERROR_CTRL_NOT_AVAILABLE		-8
#define PIPE_ERROR_INFO_NOT_AVAILABLE		-9
#define PIPE_ERROR_CHANNEL_OOB				-10

// Flags that can be passed to pipe_client_init_channel()
#define EN_PIPE_CLIENT_SIMPLE_HELPER		(1<<0) // must provide a buffer length on init
#define EN_PIPE_CLIENT_CAMERA_HELPER		(1<<1) // no need to choose a buffer length
#define EN_PIPE_CLIENT_POINT_CLOUD_HELPER	(1<<2) // must provide a buffer length on init
#define EN_PIPE_CLIENT_DEBUG_PRINTS			(1<<3) // turn on debug prints
#define EN_PIPE_CLIENT_AUTO_RECONNECT		(1<<4) // turn on auto-reconnect

/**
 * @brief      Initializes a new dedicated pipe from the server for this app and
 *             starts a background pthread to read it.
 *
 *             First, this sends the client-specific "name" string to the
 *             server's request pipe at pipe_DIR/request. The server will
 *             respond by opening a new dedicated pipe for this client at the
 *             location pipe_dir/name
 *
 *             This function waits up to a second for the server to make this
 *             pipe. It is then opened with the O_RDONLY for blocking reads and
 *             its file descriptor is saved for reading by the user either
 *             directly or with the helper thread.
 *
 *             The easiest way to get data is to enable either a simple or
 *             camera helper read thread by passing in either
 *             EN_PIPE_CLIENT_SIMPLE_HELPER or EN_PIPE_CLIENT_CAMERA_HELPER as
 *             flags. This thread will be started in the background by this
 *             function and will sit in a blocking read loop until the user
 *             calls pipe_client_close(). Each time data is available to read,
 *             the data will be passed by pointer to a user-defined callback
 *             function set with the pipe_client_set_data_cb() or
 *             pipe_client_set_camera_cb() function.
 *
 *             When using the simple data helper, you will need to specify the
 *             size of the read buffer used by this helper thread with the
 *             buf_len argument. This length will be heavily dependent on the
 *             type and frequency of data you expect to receive. The cameras
 *             helper thread will allocate just enough memory for one camera
 *             frame abd the buf_len argument is ignored.
 *
 *             Alternatively, the user can disable the helper thread by not
 *             passing an any of the helper flags and read the pipe file
 *             descriptor manually. To retrieve this file descriptor, use the
 *             pipe_client_get_fd() function.
 *
 *             Additionally, this opens the pipe_DIR/control pipe with the
 *             O_WRONLY flag if the server has enabled a control pipe for that
 *             channel. Its file descriptor is saved to the control_fd[ch]
 *             variable for the user to send control commands to the server. The
 *             client can use the pipe_send_control_cmd() function as a helper
 *             to do this.
 *
 *             Up to 16 channels can be opened to receive data from the same or
 *             multiple servers. This channel number has nothing to do with the
 *             server channel. It is only a way to index and differentiate
 *             between multiple open pipes.
 *
 * @param[in]  ch       channel to initialize (0-15)
 * @param      dir      base directory for the channel you wish to read from
 * @param      name     desired name of the dedicated pipe to be created
 * @param[in]  flags    flags to configure optional features
 * @param[in]  buf_len  The buffer size (bytes) used by the read helper thread
 *
 * @return     0 on success, -1 on failure
 */
int pipe_client_init_channel(int ch, char* dir, const char* name, int flags, int buf_len);


/**
 * @brief      print a human readable error returned by pipe_client_init_channel
 *
 *             pipe_client_init_channel fails silently so that clients can keep
 *             trying to init waiting for a server to come online or for a
 *             channel to free up without cluttering up the screen with error
 *             messages. If the user wants to print which error occured anyway
 *             then they can use this function. See the example
 *             modal-hello-client
 *
 * @param[in]  e     error returned from pipe_client_init_channel
 */
void pipe_client_print_error(int e);


/**
 * @brief      take a partial (or full) pipe directory string, and write out the
 *             full and correct version
 *
 *             pipe_client_init_channel expects a full directory path ending in
 *             '/' containing the pipe channel it should subscribe to. For
 *             example, the path to the imu0 channel the voxl-imu-server
 *             publishes is: /run/mpa/imu0/
 *
 *             However, a user may just want to provide the short name "imu0" or
 *             "imu0/" as a command line argument to a program such as
 *             voxl-test-imu. In this case, it's up to voxl-test-imu to parse
 *             that command line argument and construct the full path. A user
 *             might also want to specify a full path to somewhere other than
 *             /run/mpa/. This helper function provides this function and can be
 *             used while parsing arguments as follows:
 *
 *             char pipe_path[128] pipe_client_contruct_full_path(optarg,
 *             pipe_path);
 *
 *             Examples of input > output behavior
 *
 *             imu0     > /run/mpa/imu0/ imu0/    > /run/mpa/imu0/ /foo/bar >
 *             /foo/bar/ /foo     > /foo/
 *
 *             This does not guarantee the path exists, it only formats the
 *             string. Try to init the channel with pipe_client_init_channel to
 *             see if it's active. This function may fail if the string pointers
 *             are NULL or an empty string is provided
 *
 * @param      in    input string
 * @param      out   pointer to pre-allocated memory to write the result to
 *
 * @return     0 on success, -1 on failure
 */
int pipe_client_construct_full_path(char* in, char* out);


/**
 * @brief      Fetch the number of bytes available to read from the pipe.
 *
 *
 *             This is useful to check to see if a client is falling behind and
 *             needs to dump data to catch up. E.g. when doing heavy processing
 *             on camera frames.
 *
 *             There is an equivalent function pipe_server_bytes_in_pipe() to
 *             allow servers to check this information too.
 *
 * @param[in]  ch    channel to check
 *
 * @return     # bytes available to read or -1 on error
 */
int pipe_client_bytes_in_pipe(int ch);


/**
 * @brief      Fetch the current size (capacity) of the pipe for a given
 *             channel.
 *
 *             Pipes usually default to 64k depending on your platform. This may
 *             be too small for some applications, particularly camera frames.
 *
 *             There is an equivalent function pipe_server_get_pipe_size() to
 *             allow servers to check this information too.
 *
 * @param[in]  ch    channel to check
 *
 * @return     size of pipe in bytes, or -1 on error
 */
int pipe_client_get_pipe_size(int ch);

/**
 * @brief      Set the size of a pipe for a given channel.
 *
 *             A client can (and should!) increase the pipe size in cases where
 *             they need more buffering. Servers may also increase the pipe size
 *             if they see fit.
 *
 *             Pipes behave like FIFO buffers and can store data until ready to
 *             be read by the client. Root can set the pipe size up to 256MiB on
 *             most Linux systems. Normal users can usually set the pipe size up
 *             to 1MiB, but processes usually run as root on VOXL
 *
 *             This function returns the new size of the pipe after attempting
 *             to set it to a new desired size. Usually the kernel will
 *             "round up" to the next exponent of 2 so you may get a pipe size
 *             that's larger than requested. If you request a size that's too
 *             large and the kernel rejects the request, then the pipe size will
 *             remain the same and that existing size will be returned. You
 *             should therefore error-check by seeing if the return value is <
 *             the desired size.
 *
 *             Since the kernel will round-up anyway, you might as well just
 *             request a nice even size, e.g. 1,2,4,8,16,32,64,128, or 256Mib.
 *
 *             e.g. to set the pipe size to 16MiB use size_bytes = 16*1024*1024;
 *
 * @param[in]  ch          channel to set
 * @param[in]  size_bytes  The desired pipe size in bytes
 *
 * @return     new size of the pipe or -1 on error.
 */
int pipe_client_set_pipe_size(int ch, int size_bytes);


/**
 * @brief      assign a callback function to be called when new data is read by
 *             the simple helper thread.
 *
 *             The user-defined callback should return void and take 3
 *             arguments: the channel the data was received on, a pointer to the
 *             data, and the number of bytes read.
 *
 *             Although callback functions can be assigned individually for each
 *             channel, having the channel argument allows a single callback to
 *             be assigned for all channels and still be able to differentiate
 *             which channel the message was received on.
 *
 *             Note that this callback will only get called if the simple read
 *             helper thread was enabled when the channel was initialized with
 *             pipe_client_init_channel().
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void client_simple_cb(int ch, char* data, int bytes, void* context);
int pipe_client_set_simple_helper_cb(int ch, client_simple_cb* cb, void* context);


/**
 * @brief      assign a callback function to be called when new data is read by
 *             the camera helper thread.
 *
 *             The user-defined callback should return void and take 3
 *             arguments: the channel the data was received on, a metadata
 *             struct, and a pointer to the beginning of the frame data.
 *
 *             Although callback functions can be assigned individually for each
 *             channel, having the channel argument allows a single callback to
 *             be assigned for all channels and still be able to differentiate
 *             which channel the message was received on.
 *
 *             Note that this callback will only get called if the camera helper
 *             thread was enabled when the channel was initialized with
 *             pipe_client_init_channel().
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void client_camera_cb(int ch, camera_image_metadata_t meta, char* frame, void* context);
int pipe_client_set_camera_helper_cb(int ch, client_camera_cb* cb, void* context);


/**
 * @brief      assign a callback function to be called when new data is read by
 *             the point cloud helper thread.
 *
 *             The data provided is in the format XYZXYZXYZ... where each point
 *             consists of 3 floats. The metadata struct will indicate the
 *             number of points, a timestamp, and which reference frame the data
 *             is in.
 *
 *             The user-defined callback should return void and take 3
 *             arguments: the channel the data was received on, a metadata
 *             struct, and a pointer to the beginning of the frame data.
 *
 *             Although callback functions can be assigned individually for each
 *             channel, having the channel argument allows a single callback to
 *             be assigned for all channels and still be able to differentiate
 *             which channel the message was received on.
 *
 *             Note that this callback will only get called if the point cloud
 *             helper thread was enabled when the channel was initialized with
 *             pipe_client_init_channel().
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void client_pc_cb(int ch, point_cloud_metadata_t meta, float* data, void* context);
int pipe_client_set_point_cloud_helper_cb(int ch, client_pc_cb* cb, void* context);


/**
 * @brief      assign a callback function to be called when a connection is
 *             established with a server
 *
 *             This is really only useful in auto-reconnect mode to indicate
 *             when a connection has been re-established. however, in normal
 *             mode, this will still be called if the user sets the callback
 *             before initiating the connection and then a connection is
 *             established with pipe_client_init_channel()
 *
 *             The user-defined callback should return void and take 2
 *             arguments: the channel number and an optional context pointer
 *
 *             Although callback functions can be assigned individually for each
 *             channel, having the channel argument allows a single callback to
 *             be assigned for all channels and still be able to differentiate
 *             which channel the message was received on.
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void client_connect_cb(int ch, void* context);
int pipe_client_set_connect_cb(int ch, client_connect_cb* cb, void* context);



/**
 * @brief      assign a callback function to be called when one of the helpers
 *             disconnects from the server
 *
 *             The user-defined callback should return void and take 2
 *             arguments: the channel number and an optional context pointer
 *
 *             Although callback functions can be assigned individually for each
 *             channel, having the channel argument allows a single callback to
 *             be assigned for all channels and still be able to differentiate
 *             which channel the message was received on.
 *
 *             Note that this callback will only get called if one of the read
 *             helpers was enabled when the channel was initialized.
 *             pipe_client_init_channel().
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void client_disc_cb(int ch, void* context);
int pipe_client_set_disconnect_cb(int ch, client_disc_cb* cb, void* context);


/**
 * @brief      check if a channel is connected or not
 *
 *             This is mainly to be used with the EN_PIPE_CLIENT_AUTO_RECONNECT
 *             auto reconnect feature to check if the channel is connected or
 *             not without using the connect/disconnect callbacks.
 *
 * @param[in]  ch    channel to check (0-15)
 *
 * @return     1 if connected, 0 if not
 */
int pipe_client_is_connected(int ch);


/**
 * @brief      retrieve the data pipe file descriptor for the client to read
 *
 *             It is NOT recommended to use this function if the the read helper
 *             thread was enabled when the channel was initialized with
 *             pipe_client_init_channel(). Note that this file descriptor will
 *             be closed on a call to pipe_client_close_channel() or
 *             pipe_client_close_all();
 *
 * @param[in]  ch    channel to fetch fd for (0-15)
 *
 * @return     file descriptor
 */
int pipe_client_get_fd(int ch);

/**
 * @brief      send a control command string to the server
 *
 *             Available valid strings are server-dependent. For example the
 *             "start_calibration" string can be sent to voxl-imu-server to
 *             zero-out the current imu offsets for recalibration. This file is
 *             intended to be a generic interface for clients to use to talk to
 *             a named-pipe interface so we don't pre-define strings here.
 *
 *             Note that creation of a control pipe is optional and determined
 *             by the server. If the server never enabled a control pipe then
 *             this function will return -1.
 *
 * @param[in]  ch           channel to send the command to (0-15)
 * @param      control_cmd  The control string
 *
 * @return     0 on success, -1 on failure
 */
int pipe_client_send_control_cmd(int ch, char* control_cmd);


/**
 * @brief      Send data to the server through the control pipe.
 *
 *             The data structure is are server-dependent. This function differs
 *             from pipe_client_send_control_cmd in that the data does not have
 *             to be a string.
 *
 *             Note that creation of a control pipe is optional and determined
 *             by the server. If the server never enabled a control pipe then
 *             this function will return -1.
 *
 * @param[in]  ch     channel to send the command to (0-15)
 * @param      data   The data
 * @param[in]  bytes  The control string
 *
 * @return     0 on success, -1 on failure
 */
int pipe_client_send_control_cmd_bytes(int ch, char* data, int bytes);


/**
 * @brief      Requests up to buf_len bytes from the server's info pipe
 *             and stores it in buf.
 *
 * @param[in]  ch       channel to recieve from
 * @param      buf      The buffer
 * @param[in]  buf_len  The buffer length
 *
 * @return     number of bytes read or -1 on failure
 */
int pipe_client_get_info_string(int ch, char* buf, int buf_len);

/**
 * @brief      closes and deinitializes a single channel
 *
 *             If the read helper thread is enabled, this functions signals the
 *             thread to close. This also closes any open file descriptors
 *             including the data fd even if the client grabbed it with the
 *             pipe_client_get_fd() function.
 *
 * @param[in]  ch    channel to close (0-15)
 */
void pipe_client_close_channel(int ch);


/**
 * @brief      closes all open channels
 */
void pipe_client_close_all(void);



#ifdef __cplusplus
}
#endif

#endif // MODAL_PIPE_CLIENT_H