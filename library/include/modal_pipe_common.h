/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODAL_PIPE_COMMON_H
#define MODAL_PIPE_COMMON_H


/**
 * This is the recommended location for pipes to be created. It is not enforced
 * or used anywhere in this library, it's just where we recommend putting pipes
 * for consistency. For example, the imu server would make two channels with two
 * directories in this base dir, one for each imu0 and imu1
 *
 * - /run/mpa/imu0/
 * - /run/mpa/imu1/
 *
 * The camera server would then share the base dir, making a new dir for each of
 * the cameras:
 *
 * - /run/mpa/tracking/
 * - /run/mpa/stereo/
 * - /run/mpa/hires/
 *
 * This base directory is chosen because it only exists in memory, not on the
 * disk. It is also NOT preserved between reboots, ensuring no remnants of old
 * pipes exist on startup after improper shutdown. It can also be mounted inside
 * of docker images to share pipe data in/out of dockers.
 */
#define MODAL_PIPE_DEFAULT_BASE_DIR	"/run/mpa/"

// Sensible limits of the length of directories and paths
#define MODAL_PIPE_MAX_DIR_LEN		64
#define MODAL_PIPE_MAX_NAME_LEN		64
#define MODAL_PIPE_MAX_PATH_LEN		128



#endif // MODAL_PIPE_COMMON_H
