/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODAL_PIPE_SERVER_H
#define MODAL_PIPE_SERVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <modal_pipe_common.h>
#include <modal_pipe_interfaces.h>


// Sensible limit on the number of channels a server can advertize
#define PIPE_SERVER_MAX_CHANNELS		16
// Sensible limit on the number of clients that can connect to a single channel
#define PIPE_SERVER_MAX_CLIENTS_PER_CH	16

// Flags parsed to pipe_server_init_channel() to control behavior.
#define SERVER_FLAG_EN_CONTROL_PIPE		(1<<0)
#define SERVER_FLAG_EN_DEBUG_PRINTS		(1<<1)
#define SERVER_FLAG_EN_INFO_PIPE		(1<<2)

// Possible states for a client
#define	CLIENT_UNINITIALIZED	0	// client doesnt exist yet, no request has been made.
#define	CLIENT_INITIALIZED		1	// new pipe has been made for the client, but no data has been transfered yet
#define	CLIENT_CONNECTED		2	// client has connected and has opened the pipe for reading
#define	CLIENT_DISCONNECTED		3	// Server tried to write to the client but client didn't have the pipe open for reading


/**
 * @brief      Initialize an set up a single channel. Up to 16 channels are
 *             supported for simultaneous use. This channel number has nothing
 *             to do with the client channel. It is only a way to index and
 *             differentiate between multiple open interfaces.
 *
 *             This will create the user-specified directory and within it
 *             create a request pipe for clients to send requests to. This also
 *             starts a lightweight pthread to service the request pipe.
 *
 *             The directory string dir MUST end in a trailing '/' character
 *             followed by the usual null terminator. This function will create
 *             all necessary parent directories if missing.
 *
 *             Extra features can be turned on/off with the flags field.
 *             Currently only one extra feature is supported: the control pipe:
 *
 *             SERVER_FLAG_EN_CONTROL_PIPE
 *
 *             By enabling the control pipe, a an extra pipe is opened alongside
 *             the "request" pipe with the static name "control". Clients can
 *             send anything into this control pipe and the server can read the
 *             data via callback set by pipe_server_set_control_cb(). An example
 *             of this use case is the IMU calibrator app (a client) tells the
 *             voxl-imu-server to go into calibration mode by sending the
 *             "START_CALIBRATION" string into a control pipe.
 *
 * @param[in]  ch     channel to init (0-15)
 * @param[in]  dir    The directory to create to house the pipes
 * @param[in]  flags  extra feature flags
 *
 * @return     0 on success, -1 on failure
 */
int pipe_server_init_channel(int ch, const char* dir, int flags);


/**
 * @brief      Manually add a new client and named pipe for sending data out.
 *
 *             This is usually called automatically when a new request
 *             comes in so the server doesn't have to do this themselves. This
 *             function is make available if the server needs to create a
 *             default data pipe that is open without the need for a client
 *             requesting it.
 *
 * @param[in]  ch    channel to add a dedicated pipe to (0-15)
 * @param      name  The name of the pipe
 *
 * @return     ID number of newly added client, or -1 on failure.
 */
int pipe_server_add_client(int ch, const char* name);


/**
 * @brief      set the default pipe size that will be created when a new client
 *             connects to the specified channel.
 *
 *             The linux kernel makes pipes 64k long by default which is too
 *             small for high-data applications such as sending cameras frames.
 *             Servers which send large data packets should create pipes for
 *             clients with a more reasonable size settable by this function.
 *             Pipes can be resized later on a per-client basis with
 *             pipe_server_set_pipe_size(int ch, int client_id);
 *
 *             Pipes usually default to 64k depending on your platform. This may
 *             be too small for some applications, particularly camera frames.
 *
 *             There is an equivalent function pipe_client_get_pipe_size() to
 *             allow clients to check this information too.
 *
 *             Usually the kernel will "round up" to the next exponent of 2 so
 *             you may get a pipe size that's larger than requested. If you
 *             request a size that's too large and the kernel rejects the
 *             request, then the pipe size will remain the same. The linux
 *             kernel seems to set a limit at 256MiB but you shouldn't need a
 *             pipe that big. 4MiB is reasonable for sending VGA image streams,
 *             16MiB is reasonable for sending 4k images.
 *
 *             Since the kernel will round-up anyway, you might as well just
 *             request a nice even size, e.g. 1,2,4,8,16,32,64,128, or 256Mib.
 *
 *             e.g. to set the pipe size to 16MiB use size_bytes = 16*1024*1024;
 *
 *             Set size_bytes=0 to disable setting the pipe size and let the
 *             kernel make pipes with the kernel default (usually 64k).
 *
 * @param[in]  ch    channel to set the default for.
 *
 * @return     0 on success, -1 on failure. Note that this function will succeed
 *             in setting the default to an unreasonable value, but this doens't
 *             mean the kernel will respect that number and let the pipes be
 *             resized as expected.
 */
int pipe_server_set_default_pipe_size(int ch, int size_bytes);


/**
 * @brief      Set the string that will be made available by the info pipe.
 *
 *             Note: this string is meant for static data and should only be
 *             set once, it is possible to change after startup but you may
 *             encounter unexpected behavior
 *
 * @param[in]  ch        channel to set the string for.
 *
 * @param[in]  string    the string that will be saved to the channel
 *
 * @return     0 on success, -1 on failure.
 */

int pipe_server_set_info_string(int ch, const char* string);


/**
 * @brief      Fetch the number of bytes already in the pipe waiting to be read
 *             by the client.
 *
 *
 *             This is useful to check to see if a client is falling behind and
 *             needs to dump data to catch up. E.g. when doing heavy processing
 *             on camera frames.
 *
 *             There is an equivalent function pipe_client_bytes_in_pipe() to
 *             allow clients to check this information too.
 *
 * @param[in]  ch         channel to check
 * @param[in]  client_id  The client id
 *
 * @return     # bytes available to read or -1 on error
 */
int pipe_server_bytes_in_pipe(int ch, int client_id);


/**
 * @brief      Fetch the current size (capacity) of the pipe for a given channel
 *             and client.
 *
 * @param[in]  ch         channel to check
 * @param[in]  client_id  The client id
 *
 * @return     size of pipe in bytes, or -1 on error
 */
int pipe_server_get_pipe_size(int ch, int client_id);


/**
 * @brief      Set the size of a pipe for a given channel and client.
 *
 *             A client can (and should!) increase the pipe size in cases where
 *             they need more buffering. Servers may also increase the pipe size
 *             if they see fit. Generally servers should do this through the
 *             pipe_server_set_default_pipe_size(int ch) function instead.
 *
 *             Pipes behave like FIFO buffers and can store data until ready to
 *             be read by the client. Root can set the pipe size up to 256MiB on
 *             most Linux systems. Normal users can usually set the pipe size up
 *             to 1MiB, but processes usually run as root on VOXL
 *
 *             This function returns the new size of the pipe after attempting
 *             to set it to a new desired size. Usually the kernel will
 *             "round up" to the next exponent of 2 so you may get a pipe size
 *             that's larger than requested. If you request a size that's too
 *             large and the kernel rejects the request, then the pipe size will
 *             remain the same and that existing size will be returned. You
 *             should therefore error-check by seeing if the return value is <
 *             the desired size.
 *
 *             Since the kernel will round-up anyway, you might as well just
 *             request a nice even size, e.g. 1,2,4,8,16,32,64,128, or 256Mib.
 *
 *             e.g. to set the pipe size to 16MiB use size_bytes = 16*1024*1024;
 *
 * @param[in]  ch         channel to set
 * @param[in]  client_id  The client id
 * @param[in]  size_bytes  The desired pipe size in bytes
 *
 * @return     new size of the pipe or -1 on error.
 */
int pipe_server_set_pipe_size(int ch, int client_id, int size_bytes);


/**
 * @brief      assign a callback function to be called when a message is
 *             received on a control pipe
 *
 *             The user-defined callback should return void and take 3 arguments
 *
 *             - The channel the control message was received on. Although
 *               callback functions can be assigned individually for each
 *               channel, this allows a single callback to be assigned for all
 *               channels and still be able to differentiate which channel the
 *               message was received on.
 *
 *             - string as read from the control pipe
 *
 *             - number of bytes read fom the control pipe
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void server_control_cb(int ch, char* string, int bytes, void* context);
int pipe_server_set_control_cb(int ch, server_control_cb* cb, void* context);


/**
 * @brief      Assign a callback function to be called when a new client is
 *             added automatically as a result of a request coming in on the
 *             request pipe.
 *
 *             This is entirely optional since a new data pipe will be created
 *             automatically before this callback is executed. This exists so
 *             that the server can be aware when new clients connect and
 *             initialize hardware as necessary.
 *
 *             The user-defined callback should return void and take 4 arguments
 *
 *             - The channel the request message was received on. Although
 *               callback functions can be assigned individually for each
 *               channel, this allows a single callback to be assigned for all
 *               channels and still be able to differentiate which channel the
 *               message was received on.
 *
 *             - string as read from the request pipe, typically the name of the
 *               client
 *
 *             - number of bytes read from the request pipe
 *
 *             - client id of the newly added client
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void server_request_cb(int ch, char* string, int bytes, int client_id, void* context);
int pipe_server_set_request_cb(int ch, server_request_cb* cb, void* context);


/**
 * @brief      Assign a callback function to be called when a new client
 *             disconnects
 *
 *             This is entirely optional and only present if there server wishes
 *             to be aware of when client disconnect. Note that the server
 *             interface won't be aware that a client has disconnected until the
 *             server tries to write to it.
 *
 *             The user-defined callback should return void and take 3 arguments
 *
 *             - The channel the client disconnected from. Although callback
 *               functions can be assigned individually for each channel, this
 *               allows a single callback to be assigned for all channels and
 *               still be able to differentiate which channel the message was
 *               received on.
 *
 *             - client id that disconnected
 *
 *             - name of the client
 *
 * @param[in]  ch    channel to assign the callback to (0-15)
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void server_disconnect_cb(int ch, int client_id, char* name, void* context);
int pipe_server_set_disconnect_cb(int ch, server_disconnect_cb* cb, void* context);


/**
 * @brief      send data to all clients in one channel
 *
 * @param[in]  ch     channel to send to (0-15)
 * @param[in]  data   pointer to data to send
 * @param[in]  bytes  number of byes to send
 *
 * @return     0 on success, -1 on failure
 */
int pipe_server_send_to_channel(int ch, char* data, int bytes);


/**
 * @brief      send data to just one specified client in one channel
 *
 * @param[in]  ch         channel to send to (0-15)
 * @param[in]  client_id  client to send to (0-15)
 * @param[in]  data       pointer to data to send
 * @param[in]  bytes      number of byes to send
 *
 * @return     0 on success, -1 on failure
 */
int pipe_server_send_to_client(int ch, int client_id, char* data, int bytes);


/**
 * @brief      send camera metadata and frame data to all clients in one channel
 *
 *             This ensures each client gets exactly one metadata and one camera
 *             frame. The number of bytes read from the data pointer is pulled
 *             from the metadata struct.
 *
 *             Note that this can also be used to send stereo frames as long as
 *             the left and right images are in consecutive memory. This is
 *             possible because the size_bytes field in the
 *             camera_image_metata_t struct indicates the total size of both
 *             left and right camera frames combined. Only if the left and right
 *             frames are in separate non-consecutive buffers should you use
 *             pipe_server_send_stereo_frame_to_channel().
 *
 *             This also sets the magic number field in the metadata struct
 *             before sending so the user doesn't have to.
 *
 * @param[in]  ch    channel to send to (0-15)
 * @param[in]  meta  The camera metadata
 * @param[in]  data  pointer to data to send
 *
 * @return     0 on success, -1 on failure
 */
int pipe_server_send_camera_frame_to_channel(int ch, camera_image_metadata_t meta, char* data);


/**
 * @brief      send camera metadata and stereo frame data to all clients in one
 *             channel
 *
 *             This ensures each client gets exactly one metadata and one camera
 *             frame. The number of bytes read from the data pointer is pulled
 *             from the metadata struct.
 *
 *             This is only to be used when the left and right stereo frames are
 *             in separate non-sequential memory buffers. If the left and right
 *             frames are in one continuous memory buffer than you can just use
 *             the normal pipe_server_send_camera_frame_to_channel() function by
 *             providing a pointer to the beginning of the buffer. That will
 *             assume the left frame precedes the right frame and will execute
 *             one write to the pipe instead of two. This is possible because
 *             the size_bytes field in the camera_image_metata_t struct
 *             indicates the total size of both left and right camera frames
 *             combined.
 *
 *             Note that when constructing the metadata struct, the size_bytes
 *             field should be the size of both left and right frames combined.
 *             size_bytes/2 will be read from each pointer and sent to the pipe.
 *
 *             This also sets the magic number field in the metadata struct
 *             before sending so the user doesn't have to.
 *
 * @param[in]  ch     channel to send to (0-15)
 * @param[in]  meta   The camera metadata
 * @param      left   pointer to beginning of left frame
 * @param      right  pointer to beginning of right frame
 *
 * @return     0 on success, -1 on failure
 */
int pipe_server_send_stereo_frame_to_channel(int ch, camera_image_metadata_t meta, char* left, char* right);


/**
 * @brief      send point cloud metadata and point data to all clients in one
 *             channel
 *
 *             This ensures each client gets exactly one metadata and one point
 *             cloud. The number of bytes read from the data pointer is
 *             3*sizeof(float)*meta.n_points
 *
 *             This also sets the magic number field in the metadata struct
 *             before sending so the user doesn't have to.
 *
 * @param[in]  ch    channel to send to (0-15)
 * @param[in]  meta  The point cloud metadata
 * @param[in]  data  pointer to data to send
 *
 * @return     0 on success, -1 on failure
 */
int pipe_server_send_point_cloud_to_channel(int ch, point_cloud_metadata_t meta, float* data);



/**
 * @brief      check the state of a particular client
 *
 * @param[in]  ch         The channel to check
 * @param[in]  client_id  The client id to check
 *
 * @return     The pipe client state, see CLIENT_CONNECTED etc
 */
int pipe_server_get_client_state(int ch, int client_id);


/**
 * @brief      fetch the number of clients currently connected to a particular
 *             channel
 *
 *             This includes clients that have sent a request but have not yet
 *             received their first packet.
 *
 * @param[in]  ch    channel to check
 *
 * @return     The total number of connected clients
 */
int pipe_server_get_num_clients(int ch);


/**
 * @brief      get the client id associated with a certain name
 *
 * @param[in]  ch    channel to check in
 * @param      name  The desired name
 *
 * @return     Returns the associate client id if it exists, otherwise -1
 */
int pipe_server_get_client_id_from_name(int ch, char* name);


/**
 * @brief      get the name associated with a particular client is
 *
 * @param[in]  ch         channel to check in
 * @param      client_id  The desired client id
 *
 * @return     Returns the associate name as a string if it exists, otherwise
 *             NULL
 */
char* pipe_server_get_client_name_from_id(int ch, int client_id);


/**
 * @brief      closes and deinitializes a single channel
 *
 *             This removes the named pipes from the file system so clients are
 *             informed the server has closed. If a client want to reconnect to
 *             the server after this they must send a new request.
 *
 * @param[in]  ch    channel to close (0-15)
 */
void pipe_server_close_channel(int ch);


/**
 * @brief      closes and deinitializes all channels
 *
 *             This removes the named pipes from the file system so clients are
 *             informed the server has closed. If a client want to reconnect to
 *             the server after this they must send a new request.
 */
void pipe_server_close_all(void);


/**
 * @brief      closes and deinitializes all channels
 *
 *             This removes the named pipes from the file system so clients are
 *             informed the server has closed. If a client want to reconnect to
 *             the server after this they must send a new request.
 */
__attribute__((deprecated("\nPlease use pipe_server_close_all() instead")))
void pipe_server_close_all_channels(void);


#ifdef __cplusplus
}
#endif

#endif // MODAL_PIPE_SERVER_H