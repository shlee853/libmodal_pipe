/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODAL_PIPE_SINK_H
#define MODAL_PIPE_SINK_H

#ifdef __cplusplus
extern "C" {
#endif

#include <modal_pipe_common.h>


// Sensible limit on the number sinks a process can create
#define PIPE_SINK_MAX_CHANNELS	16

// Flags that can be passed to pipe_sink_init_channel()
#define EN_PIPE_SINK_SIMPLE_HELPER		(1<<0) // must provide a buffer length on init
#define EN_PIPE_SINK_DEBUG_PRINTS		(1<<1)

/**
 * @brief      Creates a new named pipe at the specified path to act as a sink
 *             for other processes to send data into.
 *
 *             It is opened for blocking reads and its file descriptor is saved
 *             for reading by the user either directly or with the helper
 *             thread.
 *
 *             The easiest way to get data is to enable the helper read thread
 *             by setting the EN_PIPE_SINK_SIMPLE_HELPER bit in the flags
 *             argument. This thread will be started in the background by this
 *             function and will sit in a blocking read loop until the user
 *             calls pipe_sink_close_channel() or pipe_sink_close_all(). Each
 *             time data is available to read, the data will be passed by
 *             pointer to a user-defined callback function set with the
 *             pipe_sink_set_data_cb() function.
 *
 *             You will need to specify the size of the read buffer used by this
 *             helper thread with the buf_len argument. This length will be
 *             heavily dependent on the type and frequency of data you expect to
 *             receive. The buffer is dynamically allocated on the heap.
 *
 *             Alternatively, the user can disable the helper thread and read
 *             the pipe file descriptor manually. To retrieve this file
 *             descriptor, use the pipe_sink_get_fd() function.
 *
 * @param[in]  ch       channel to initialize
 * @param      path     path for the pipe to be created
 * @param[in]  flags    flags to configure optional features
 * @param[in]  buf_len  The buffer size (bytes) used by the read helper thread
 *
 * @return     0 on success, -1 on failure
 */
int pipe_sink_init_channel(int ch, char* path, int flags, int buf_len);


/**
 * @brief      Assign a callback function to be called when new data is read by
 *             the read helper thread.
 *
 *             The user-defined callback should return void and take 3
 *             arguments: the channel the data was received on, a pointer to the
 *             data, and the number of bytes read.
 *
 *             Although callback functions can be assigned individually for each
 *             channel, having the channel argument allows a single callback to
 *             be assigned for all channels and still be able to differentiate
 *             which channel the message was received on.
 *
 *             Note that this callback will only get called if the read helper
 *             thread was enabled when the channel was initialized with
 *             pipe_sink_init_channel().
 *
 * @param[in]  ch    channel to assign the callback to
 * @param[in]  func  The function pointer
 *
 * @return     0 on success, -1 on failure
 */
typedef void sink_simple_cb(int ch, char* data, int bytes, void* context);
int pipe_sink_set_simple_cb(int ch, sink_simple_cb* cb, void* context);


/**
 * @brief      retrieve the data pipe file descriptor for the client to read
 *
 *             It is NOT recommended to use this function if the the read helper
 *             thread was enabled when the channel was initialized with
 *             pipe_sink_init_channel(). Note that this file descriptor will
 *             be closed on a call to pipe_sink_close_channel() or
 *             pipe_sink_close_all();
 *
 * @param[in]  ch    channel to fetch fd for
 *
 * @return     file descriptor
 */
int pipe_sink_get_fd(int ch);


/**
 * @brief      closes and deinitializes a single channel
 *
 *             If the read helper thread is enabled, this functions signals the
 *             thread to close. This also closes and deletes the pipe even if
 *             the client grabbed its file descriptor with the
 *             pipe_client_get_fd() function.
 *
 * @param[in]  ch    channel to close
 */
void pipe_sink_close_channel(int ch);


/**
 * @brief      closes all open channels
 */
void pipe_sink_close_all(void);



#ifdef __cplusplus
}
#endif

#endif // MODAL_PIPE_SINK_H