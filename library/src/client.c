/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#define _GNU_SOURCE // for pthread_timed_join, F_GETPIPE_SZ, and F_SETPIPE_SZ
#include <stdio.h>	// for fprintf
#include <unistd.h>	// for read() & write()
#include <errno.h>	// to check error in read() and write()
#include <sys/ioctl.h>
#include <fcntl.h>	// for O_WRONLY & O_RDONLY
#include <string.h>	// for strlen()
#include <stdlib.h>	// for malloc
#include <pthread.h>
#include <signal.h>	// for pthread_kill
#include <limits.h>	// for PATH_MAX

#include <modal_pipe_client.h>
#include "misc.h"

// shorten these defines to improve code readability
#define N_CH		PIPE_CLIENT_MAX_CHANNELS
#define DIR_LEN		MODAL_PIPE_MAX_DIR_LEN
#define NAME_LEN	MODAL_PIPE_MAX_NAME_LEN
#define PATH_LEN	MODAL_PIPE_MAX_PATH_LEN

// sensible limit on number of duplicate names per client
#define MAX_NAMES	8

// struct to define the state of each channel
typedef struct client_channel_t{
	int					running;			///< flag to indicate if helper thread should shut down
	int					data_fd;			///< this is the file descriptor to read data from
	int					control_fd;			///< one control pipe per channel, store fd here
	char*				buf;				///< read buffers on heap
	int					buf_len;			///< length of read buffers
	char				pipe_dir[DIR_LEN];	///< path to server's pipe directory
	char				name[NAME_LEN];		///< client name
	char				req_path[PATH_LEN];	///< path to request pipe
	char				data_path[PATH_LEN];///< path to data pipe
	int					flags;				///< copy of flags passed to client_init function
	pthread_t			helper_thread_id;	///< only one helper pthread per channel
	int					helper_enabled;		///< flag to indicate if we started a helper thread
	client_simple_cb*	simple_cb;			///< callback for the simple helper
	client_camera_cb*	camera_cb;			///< callback for the camera helper
	client_pc_cb*		point_cb;			///< callback for the point cloud helper
	client_connect_cb*	connect_cb;			///< optional callback set when server connects
	client_disc_cb*		disconnect_cb;		///< optional callback set when server disconnects
	void*				cb_context;			///< context pointer shared by all callbacks
} client_channel_t;

// array of structs defining the state of each channel
static client_channel_t c[N_CH];
// set to 1 to enable debug prints
static int en_debug;
// each channel gets a mutex to protect during init or cleanup
static pthread_mutex_t mtx[N_CH];


// reset a channel to all 0's as if the program just started
// don't lock the mutex in here, the calling function should do that
static void _clean_channel(int ch)
{
	if(ch<0 || ch>=N_CH) return;

	// free the read buffer if allocated
	if(c[ch].buf != NULL){
		if(en_debug) printf("freeing buffer for client channel %d\n", ch);
		free(c[ch].buf);
		c[ch].buf = 0;
		c[ch].buf_len = 0;
	}

	// close and cleanup file descriptors
	if(en_debug) printf("closing FDs for client channel %d\n", ch);
	if(c[ch].data_fd){
		close(c[ch].data_fd);
		c[ch].data_fd = 0;
	}
	if(c[ch].control_fd){
		close(c[ch].control_fd);
		c[ch].control_fd = 0;
	}

	// zero out static stuff
	c[ch].running = 0;
	memset(c[ch].pipe_dir, 0, DIR_LEN);
	memset(c[ch].name, 0, NAME_LEN);
	memset(c[ch].data_path, 0, PATH_LEN);
	memset(c[ch].req_path, 0, PATH_LEN);
	c[ch].flags = 0;
	c[ch].helper_thread_id = 0;
	c[ch].helper_enabled = 0;

	// All that's left are callbacks which we should leave alone so the user
	// only needs to set them once without them getting wiped.
	return;
}


// wrapper for access() to check if a file exists to make code more readable
static int _exists(char* path)
{
	// file exists
	if(access(path, F_OK ) != -1 ) return 1;
	// file doesn't exist
	return 0;
}


// dummy function to catch the USR1 signal
static void _sigusr_cb(__attribute__((unused)) int sig)
{
	if(en_debug) printf("helper thread received sigusr %d\n", sig);
	return;
}


/**
 * @brief      read from a pipe with error checks
 *
 *             used by the helper thread to consense the code.
 *
 * @param[in]  ch             channel
 * @param      buf            The buffer
 * @param[in]  bytes_to_read  The bytes to read
 *
 * @return     -1 if helper should go back to beginning of the loop, otherwise
 *             returns the number of bytes read
 */
static int _read_helper(int ch, char* buf, int bytes_to_read)
{
	int bytes_read;

	if(c[ch].data_fd == 0){
		if(en_debug){
			fprintf(stderr, "channel %d helper tried to read from closed fd\n", ch);
		}
		return -1;
	}

	if(buf == NULL){
		fprintf(stderr, "ERROR channel %d helper tried to read into NULL buffer\n", ch);
		return -1;
	}

	// try a read
	bytes_read = read(c[ch].data_fd, buf, bytes_to_read);

	// quickly check if running changed while we were reading
	if(!c[ch].running){
		if(en_debug){
			printf("helper thread for channel %d stopping by request\n", ch);
		}
		return -1;
	}

	// do a little error handling
	if(bytes_read<=0){

		// helpful print showing read() actually told us
		if(en_debug){
			printf("read returned %d, errno: %d, server likely disconnected\n", bytes_read, errno);
			perror("errno=");
		}

		// server disconnected or some other unexpected error, if not in
		// auto-reconnect mode just exit the thread
		if(!(c[ch].flags & EN_PIPE_CLIENT_AUTO_RECONNECT)){
			c[ch].running = 0;
		}

		// close file descriptors to indicate the disconnect
		if(c[ch].data_fd!=0){
			close(c[ch].data_fd);
			c[ch].data_fd = 0;
		}
		if(c[ch].control_fd!=0){
			close(c[ch].control_fd);
			c[ch].control_fd = 0;
		}

		// inform client of the disconnect
		if(c[ch].disconnect_cb){
			c[ch].disconnect_cb(ch, c[ch].cb_context);
		}
		return -1;
	}

	return bytes_read;
}


static int _check_cam_meta(int ch, camera_image_metadata_t meta, int* bytes_to_read)
{
	// indicate there is nothing to read untill we pass all of our checks
	*bytes_to_read = 0;

	// validate the packet magic number
	if(meta.magic_number != CAMERA_MAGIC_NUMBER){
		fprintf(stderr, "ERROR: invalid metadata, magic number=%d, expected %d\n", meta.magic_number, CAMERA_MAGIC_NUMBER);
		return -1;
	}

	// also check that the size makes sense since we are about to allocate
	// size_bytes on the heap and this could go wrong
	if(meta.size_bytes > (meta.width*meta.height*10)){
		fprintf(stderr, "ERROR: received unreasonably large camera frame size\n");
		return -1;
	}

	// allocate a buffer for the frame if it's not allocated already
	if(c[ch].buf == NULL){
		c[ch].buf_len = meta.size_bytes;
		c[ch].buf = malloc(meta.size_bytes);
		if(c[ch].buf==NULL){
			perror("ERROR: allocating memory for image buffer");
			return -1;
		}
	}

	// Realloc more memory if the frame size grew
	if(c[ch].buf_len<meta.size_bytes){
		c[ch].buf = realloc(c[ch].buf, meta.size_bytes);
		if(c[ch].buf==NULL){
			perror("ERROR: reallocating memory for image buffer");
			printf("requested frame buffer was %d bytes\n", meta.size_bytes);
			return -1;
		}
		c[ch].buf_len = meta.size_bytes;
	}

	// passed all checks, indicate there is data to read
	*bytes_to_read = meta.size_bytes;
	return 0;
}


static int _check_point_meta(int ch, point_cloud_metadata_t meta, int* bytes_to_read)
{
	// indicate there is nothing to read untill we pass all of our checks
	*bytes_to_read = 0;

	if(meta.magic_number != POINT_CLOUD_MAGIC_NUMBER){
		fprintf(stderr, "invalid metadata, magic number=%d, expected %d\n", meta.magic_number, POINT_CLOUD_MAGIC_NUMBER);
		return -1;
	}

	// next to allocate or reallocate the read buffer if necessary, see how many
	// bytes we need to fit in the point cloud
	int size_bytes = meta.n_points * 3 * sizeof(float);

	// allocate a buffer if it's not allocated already
	if(c[ch].buf == NULL){
		c[ch].buf_len = size_bytes;
		c[ch].buf = malloc(size_bytes);
		if(c[ch].buf==NULL){
			perror("ERROR: allocating memory for point cloud");
			return -1;
		}
	}

	// Realloc more memory if the size grew
	if(c[ch].buf_len<size_bytes){
		c[ch].buf = realloc(c[ch].buf, size_bytes);
		if(c[ch].buf==NULL){
			perror("ERROR: reallocating memory for point cloud");
			printf("requested buffer was %d bytes\n", size_bytes);
			return -1;
		}
		c[ch].buf_len = size_bytes;
	}

	// passed all checks, indicate there is data to read
	*bytes_to_read = size_bytes;
	return 0;
}

static int _connect_to_server(int ch)
{
	// Check server is online by seeing if request pipe exists
	if(!_exists(c[ch].req_path)) return PIPE_ERROR_SERVER_NOT_AVAILABLE;

	// passed all the validity checks, lock the mutex so we can start
	pthread_mutex_lock(&mtx[ch]);

	// find next index open for that name. e/g/ client0, client1, client2...
	// up to MAX_NAMES
	char newpath[PATH_LEN];
	char newname[NAME_LEN+16];
	int  newname_len = 0;
	int i;
	for(i=0;i<MAX_NAMES;i++){
		// construct a new path with the current dir, name, and index i
		sprintf(newpath,"%s%s%d", c[ch].pipe_dir, c[ch].name, i);
		if(!_exists(newpath)){
			// name with this index doesn't exist yet, good, use it!
			newname_len = sprintf(newname, "%s%d", c[ch].name, i);
			strcpy(c[ch].data_path, newpath);
			break;
		}
	}

	// error if we quit the for loop without finding an open name
	if(newname_len == 0){
		pthread_mutex_unlock(&mtx[ch]);
		return PIPE_ERROR_REACHED_MAX_NAME_INDEX;
	}

	if(en_debug){
		printf("requesting name %s\n", newname);
		printf("with complete path %s\n", newpath);
	}

	// open request pipe for writing
	int request_pipe_fd = open(c[ch].req_path, O_WRONLY|O_NONBLOCK);
	if(request_pipe_fd <= 0){
		perror("ERROR in pipe_client_init_channel opening request pipe");
		if(errno==ENXIO){
			fprintf(stderr, "Most likely the server stopped without cleaning up\n");
		}
		pthread_mutex_unlock(&mtx[ch]);
		return PIPE_ERROR_FILE_IO;
	}

	// send in our request with the desired name and close when done
	int bytes_written = write(request_pipe_fd, newname, newname_len+1);
	if(bytes_written != (newname_len+1)){
		perror("ERROR in pipe_client_init_channel writing to request pipe");
		pthread_mutex_unlock(&mtx[ch]);
		return PIPE_ERROR_FILE_IO;
	}
	close(request_pipe_fd); // don't need request pipe anymore

	// try to open the control pipe while we wait for server to service the
	// request. The control pipe is optional and may be missing which is fine.
	int dirlen = strlen(c[ch].pipe_dir);
	char control_path[dirlen+1+7];
	strcpy(control_path,c[ch].pipe_dir);
	strcat(control_path,"control");
	int ctrl_fd = open(control_path, O_WRONLY);
	if(ctrl_fd<=0){
		// no problem if control doesn't exist. server just never enabled it
		if(errno!=ENOENT){
			perror("ERROR in pipe_client_init_channel opening control pipe");
			pthread_mutex_unlock(&mtx[ch]);
			return PIPE_ERROR_FILE_IO;
		}
	}
	else{
		c[ch].control_fd = ctrl_fd;
	}

	// wait for new pipe to be created by the server
	// timeout after 1 second
	const int attempts = 10;
	for(i=0;i<attempts;i++){
		c[ch].data_fd = open(c[ch].data_path, O_RDONLY);
		if(c[ch].data_fd>0) break;
		usleep(1000000/attempts);
	}

	// check if we failed after several attempts
	if(c[ch].data_fd<=0){
		if(c[ch].control_fd) close(c[ch].control_fd);
		c[ch].control_fd = 0;
		c[ch].data_fd = 0;
		pthread_mutex_unlock(&mtx[ch]);
		return PIPE_ERROR_TIMEOUT;
	}

	// Now we have our own data pipe fd open!
	if(en_debug) printf("connected after %d attempt(s)\n", i+1);
	pthread_mutex_unlock(&mtx[ch]);

	// run the connect callback if set
	if(c[ch].connect_cb){
		c[ch].connect_cb(ch, c[ch].cb_context);
	}

	return 0;
}



// function for simple helper thread
static void* _helper_func(void* context)
{
	int ch = (long)context;
	int bytes_read;
	int bytes_to_read;
	camera_image_metadata_t cam_meta;
	point_cloud_metadata_t point_meta;

	// flags indicating which helper mode we are in
	int is_simple_helper = c[ch].flags & EN_PIPE_CLIENT_SIMPLE_HELPER;
	int is_camera_helper = c[ch].flags & EN_PIPE_CLIENT_CAMERA_HELPER;
	int is_point_helper  = c[ch].flags & EN_PIPE_CLIENT_POINT_CLOUD_HELPER;

	// catch the SIGUSR1 signal which we use to quit the blocking read
	struct sigaction action;
	action.sa_handler = _sigusr_cb;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;
	sigaction(SIGUSR1, &action, NULL);

	if(en_debug) printf("starting helper thread for channel %d\n", ch);

	// simple helper needs a read buffer, the other helpers will figure out how
	// to allocate their buffers based on metadata later
	if(is_simple_helper && c[ch].buf == 0){
		c[ch].buf = malloc(c[ch].buf_len);
		if(c[ch].buf==NULL){
			perror("ERROR: allocating memory for simple helper");
			return NULL;
		}
	}

	// primary helper loop, manages reading the pipe with different checks and
	// behaviors depending on which helper mode we are in
	while(c[ch].running){

		// first check is a pipe open, we may need to connect/reconnect
		if(c[ch].data_fd==0 && (c[ch].flags & EN_PIPE_CLIENT_AUTO_RECONNECT)){
			int ret = _connect_to_server(ch);
			if(ret<0){
				if(en_debug){
					pipe_client_print_error(ret);
					printf("going to sleep, will try to reconnect shortly\n");
				}
				usleep(500000);
				continue;
			}
		}

		// first read from pipe. This is it for the simple helper, but other
		// helpers may make multiple reads after this.
		if(is_simple_helper){
			bytes_read = _read_helper(ch, c[ch].buf, c[ch].buf_len);
		}
		else if(is_camera_helper){
			bytes_read = _read_helper(ch, (char*)&cam_meta, sizeof(camera_image_metadata_t));
		}
		else if(is_point_helper){
			bytes_read = _read_helper(ch, (char*)&point_meta, sizeof(point_cloud_metadata_t));
		}

		// go back to beginning of the loop if the read helper says to do so
		if(bytes_read<=0) continue;

		// simple helper is done here, send callback if requested
		if(is_simple_helper){
			if(c[ch].simple_cb){
				c[ch].simple_cb(ch, c[ch].buf, bytes_read, c[ch].cb_context);
			}
			continue;
		}

		// camera and point cloud helpers just read in metadata, so check it
		if(is_camera_helper && _check_cam_meta(ch, cam_meta, &bytes_to_read)<0){
			continue;
		}
		else if(is_point_helper && _check_point_meta(ch, point_meta, &bytes_to_read)<0){
			continue;
		}

		// now read the data, this may take multiple reads, for example stereo
		// frames may come in left, then right. YUV color images may come in as
		// Y first, followed by the UV bytes. This should only take 1 or 2 reads
		// if the server makes 1 or 2 writes. Try 10 times to be sure.
		int total_read = 0;
		int tries = 0;
		while(c[ch].running && tries<10 && total_read<bytes_to_read){
			// try a read just like before
			bytes_read = _read_helper(ch, c[ch].buf+total_read, bytes_to_read-total_read);
			// break on error, since we didn't read in the data this will send
			// us back to the beginning of the primary (outer) while loop.
			if(bytes_read<0) break;
			// keep track of bytes read and how many tries it's taken
			total_read += bytes_read;
			tries++;
		}

		// check if we failed to read enough bytes after 10 attempts
		if(total_read != bytes_to_read){
			fprintf(stderr, "ERROR: only read %d bytes of data, expected %d\n", bytes_to_read, total_read);
			continue;
		}

		// send callbacks if set!
		if(is_camera_helper && c[ch].camera_cb){
			c[ch].camera_cb(ch, cam_meta, c[ch].buf, c[ch].cb_context);
		}
		else if(is_point_helper && c[ch].point_cb){
			c[ch].point_cb(ch, point_meta, (float*)c[ch].buf, c[ch].cb_context);
		}

	} // end of helper while loop!


	if(en_debug){
		printf("Exiting helper thread for channel %d\n", ch);
	}

	return NULL;
}





int pipe_client_init_channel(int ch, char* dir, const char* name, int flags, int buf_len)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	if(c[ch].running){
		fprintf(stderr, "ERROR in %s, channel %d already running\n", __FUNCTION__, ch);
		return PIPE_ERROR_OTHER;
	}
	// helper specific sanity checks
	if((flags & EN_PIPE_CLIENT_SIMPLE_HELPER) && (buf_len<1)){
		fprintf(stderr, "ERROR in %s, buffer length should be >0\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	if((flags & EN_PIPE_CLIENT_POINT_CLOUD_HELPER) && (buf_len<1)){
		fprintf(stderr, "ERROR in %s, buffer length should be >0\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	// make sure multiple helpers aren't enabled
	int n_helpers = 0;
	if(flags & EN_PIPE_CLIENT_SIMPLE_HELPER)		n_helpers++;
	if(flags & EN_PIPE_CLIENT_CAMERA_HELPER)		n_helpers++;
	if(flags & EN_PIPE_CLIENT_POINT_CLOUD_HELPER)	n_helpers++;
	if(n_helpers>1){
		fprintf(stderr, "ERROR in %s, can't enable multiple helpers\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	// make sure auto-reconnect is not enabled without a helper
	if((flags & EN_PIPE_CLIENT_AUTO_RECONNECT) && n_helpers<1){
		fprintf(stderr, "ERROR in %s, can't enable multiple helpers\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}

	// turn on debugging if requested
	if(flags & EN_PIPE_CLIENT_DEBUG_PRINTS) en_debug = 1;

	// validity checking
	int dirlen = strlen(dir);
	if(dirlen<1){
		fprintf(stderr, "ERROR in %s, empty directory string provided\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	if(dirlen>=PATH_LEN){
		fprintf(stderr, "ERROR in %s, directory string is too long\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	if(dir[dirlen-1]!='/'){
		fprintf(stderr, "ERROR in %s, directory string should end in '/'\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	int namelen = strlen(name);
	if(namelen<1){
		fprintf(stderr, "ERROR in %s, empty name string provided\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	if(namelen>=(NAME_LEN-1)){
		// check >= NAME_LEN-1 since we will append a digit to the end later
		fprintf(stderr, "ERROR in %s, name string is too long\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	if(strchr(name, '/')!=NULL){
		fprintf(stderr, "ERROR in %s, name string can't contain '/'\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}

	// passed our sanity checks, now start setting up the channel
	pthread_mutex_lock(&mtx[ch]);
	strcpy(c[ch].pipe_dir, dir);
	strcpy(c[ch].name, name);
	c[ch].flags = flags;

	// construct request path for later use
	strcpy(c[ch].req_path,dir);
	strcat(c[ch].req_path,"request");

	// simple helper uses user-specified buffer length. Other helpers may
	// realloc the buffer but will use the user-specified buffer size as a
	// starting point
	c[ch].buf_len = buf_len;
	pthread_mutex_unlock(&mtx[ch]);

	// When not in auto-reconnect mode, try to connect to the server now and
	// return the error that caused the failure.
	// The helper thread will do that for us in auto-reconnect, mode.
	if(!(flags & EN_PIPE_CLIENT_AUTO_RECONNECT)){
		int ret = _connect_to_server(ch);
		if(ret<0){
			_clean_channel(ch);
			return ret;
		}
	}

	// mark channel as running, switch to 0 to tell potential helper threads
	// that we should shut down. This also indicates the channel has been
	// initialized
	c[ch].running = 1;

	// start a helper if requested
	if(n_helpers>0){
		pthread_mutex_lock(&mtx[ch]);
		pthread_attr_t attr;
		pthread_attr_init(&attr);
		c[ch].helper_enabled = 1;
		if(en_debug) printf("spawning helper pthread\n");
		pthread_create(&c[ch].helper_thread_id, &attr, _helper_func, (void*)(long)ch);
		pthread_attr_destroy(&attr);
		pthread_mutex_unlock(&mtx[ch]);
	}
	else{
		// no helper enabled!!
		c[ch].helper_enabled = 0;
	}

	return 0;
}


void pipe_client_print_error(int e)
{
	if(e == PIPE_ERROR_SERVER_NOT_AVAILABLE){
		fprintf(stderr, "Server not available\n");
	}
	else if(e == PIPE_ERROR_REACHED_MAX_NAME_INDEX){
		fprintf(stderr, "ERROR: Reached maximum number of clients with the same name\n");
	}
	else if(e == PIPE_ERROR_FILE_IO){
		fprintf(stderr, "ERROR: File IO\n");
	}
	else if(e == PIPE_ERROR_TIMEOUT){
		fprintf(stderr, "ERROR: timeout waiting for server\n");
	}
	else if(e == PIPE_ERROR_OTHER){
		fprintf(stderr, "ERROR: other\n");
	}
	else if(e == PIPE_ERROR_INVALID_ARG){
		fprintf(stderr, "ERROR: Invalid Argument\n");
	}
	else if(e == PIPE_ERROR_NOT_CONNECTED){
		fprintf(stderr, "ERROR: not connected\n");
	}
	else if(e == PIPE_ERROR_CTRL_NOT_AVAILABLE){
		fprintf(stderr, "ERROR: control pipe not available\n");
	}
	else if(e == PIPE_ERROR_INFO_NOT_AVAILABLE){
		fprintf(stderr, "ERROR: info pipe not available\n");
	}
	else if(e == PIPE_ERROR_CHANNEL_OOB){
		fprintf(stderr, "ERROR: channel out of bounds\n");
	}
	else if(e < 0){
		fprintf(stderr, "ERROR: unknown error\n");
	}
	// note, there is no final else here so that the client can call this
	// function even when there is no error (e>=0) and nothing will print out.
	return;
}


int pipe_client_construct_full_path(char* in, char* out)
{
	// sanity checks
	if(in==NULL || out==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}

	int len = strlen(in);

	// TODO test for more edge cases
	if(len<1){
		fprintf(stderr, "ERROR in %s, recevied empty string\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}
	if(len==1 && in[0]=='/'){
		fprintf(stderr, "ERROR in %s, pipe path can't just be root '/'\n", __FUNCTION__);
		return PIPE_ERROR_INVALID_ARG;
	}

	// if user didn't provide a full path starting with '/' then prepend the
	// default path and write out, recording the new length.
	if(in[0]!='/'){
		len = sprintf(out,"%s%s", MODAL_PIPE_DEFAULT_BASE_DIR, in);
	}
	// otherwise just write out the full path provided
	else len = sprintf(out,"%s", in);

	// make sure the path ends in '/' since it should be a directory
	if(out[len-1]!='/'){
		out[len]='/';
		out[len+1]=0;
	}

	return 0;
}


int pipe_client_bytes_in_pipe(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	if(!c[ch].data_fd){
		fprintf(stderr, "ERROR in %s, channel %d not initialized yet\n", __FUNCTION__, ch);
		return PIPE_ERROR_NOT_CONNECTED;
	}

	// lock mutex before working on the pipe fd
	pthread_mutex_lock(&mtx[ch]);

	// use ioctl to find bytes in the pipe
	int n_bytes;
	int ret = ioctl(c[ch].data_fd, FIONREAD, &n_bytes);
	if(ret){
		perror("ERROR in pipe_client_bytes_in_pipe");
		n_bytes = PIPE_ERROR_FILE_IO;
	}

	// done, unlock and return
	pthread_mutex_unlock(&mtx[ch]);
	return n_bytes;
}


int pipe_client_get_pipe_size(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	if(!c[ch].data_fd){
		fprintf(stderr, "ERROR in %s, channel %d not initialized yet\n", __FUNCTION__, ch);
		return PIPE_ERROR_NOT_CONNECTED;
	}

	pthread_mutex_lock(&mtx[ch]);
	int ret = fcntl(c[ch].data_fd, F_GETPIPE_SZ);
	pthread_mutex_unlock(&mtx[ch]);
	return ret;
}


int pipe_client_set_pipe_size(int ch, int size_bytes)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	if(!c[ch].data_fd){
		fprintf(stderr, "ERROR in %s, channel %d not initialized yet\n", __FUNCTION__, ch);
		return PIPE_ERROR_NOT_CONNECTED;
	}

	// use fctl with mutex protection
	pthread_mutex_lock(&mtx[ch]);
	errno = 0;
	int new_size = fcntl(c[ch].data_fd, F_SETPIPE_SZ, size_bytes);
	pthread_mutex_unlock(&mtx[ch]);

	// error check
	if(new_size<size_bytes){
		perror("ERROR failed to set pipe size");
		if(errno == EPERM){
			fprintf(stderr, "You may need to be root to make a pipe that big\n");
		}
		// if fcntl fails, it may return 0 instead of the actual size, so fetch
		// the new size and return that instead.
		return pipe_client_get_pipe_size(ch);
	}

	// if fcntl was successful it returned the new size in bytes, so return it
	return new_size;
}


int pipe_client_set_simple_helper_cb(int ch, client_simple_cb* cb, void* context)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].cb_context = context;
	c[ch].simple_cb = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_client_set_camera_helper_cb(int ch, client_camera_cb* cb, void* context)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].cb_context = context;
	c[ch].camera_cb = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_client_set_point_cloud_helper_cb(int ch, client_pc_cb* cb, void* context)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].cb_context = context;
	c[ch].point_cb = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}

int pipe_client_set_connect_cb(int ch, client_connect_cb* cb, void* context)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].cb_context = context;
	c[ch].connect_cb = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_client_set_disconnect_cb(int ch, client_disc_cb* cb, void* context)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	pthread_mutex_lock(&mtx[ch]);
	c[ch].cb_context = context;
	c[ch].disconnect_cb = cb;
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_client_is_connected(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return 0;
	}

	// start assuming we are disconnected
	int ret = 0;

	// simple check if the pipe file descriptor is valid. If we disconnected
	// then the helper thread would have set it to 0
	pthread_mutex_lock(&mtx[ch]);
	if(c[ch].data_fd>0){
		ret=1;
	}
	pthread_mutex_unlock(&mtx[ch]);
	return ret;
}


int pipe_client_get_fd(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}

	// lock the mutex before poking at the data struct
	pthread_mutex_lock(&mtx[ch]);

	// return the fd or -1 if not initialized yet
	int ret;
	if(c[ch].data_fd>0){
		ret = c[ch].data_fd;
	}
	else{
		fprintf(stderr, "ERROR in %s, channel not initialized yet\n", __FUNCTION__);
		ret = -1;
	}

	// unlock and return
	pthread_mutex_unlock(&mtx[ch]);
	return ret;
}


int pipe_client_send_control_cmd(int ch, char* cmd)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	if(c[ch].data_fd==0){
		return PIPE_ERROR_NOT_CONNECTED;
	}
	if(c[ch].control_fd==0){
		return PIPE_ERROR_CTRL_NOT_AVAILABLE;
	}
	pthread_mutex_lock(&mtx[ch]);
	int len = strlen(cmd)+1;
	if(write(c[ch].control_fd, cmd, len)!=len){
		perror("ERROR writing to control pipe");
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_client_send_control_cmd_bytes(int ch, char* data, int bytes)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
	if(c[ch].data_fd==0){
		return PIPE_ERROR_NOT_CONNECTED;
	}
	if(c[ch].control_fd==0){
		return PIPE_ERROR_CTRL_NOT_AVAILABLE;
	}
	// simple write to our open fd
	pthread_mutex_lock(&mtx[ch]);
	if(write(c[ch].control_fd, data, bytes)!=bytes){
		perror("ERROR writing to control pipe");
		pthread_mutex_unlock(&mtx[ch]);
		return -1;
	}
	pthread_mutex_unlock(&mtx[ch]);
	return 0;
}


int pipe_client_get_info_string(int ch, char* buf, int buf_len)
{
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return PIPE_ERROR_CHANNEL_OOB;
	}
#ifndef __ANDROID__
	if(c[ch].data_fd==0){
		return PIPE_ERROR_NOT_CONNECTED;
	}
#endif

	char info_path[PATH_LEN+16];
	strcpy(info_path, c[ch].pipe_dir);
	strcat(info_path,"info");

	int fd = open(info_path, O_RDONLY);
	if(fd < 0){
		return PIPE_ERROR_INFO_NOT_AVAILABLE;
	}
	int num_bytes = read(fd, buf, buf_len);
	close(fd);

	return num_bytes;
}


void pipe_client_close_channel(int ch)
{
	// sanity checks
	if(ch<0 || ch>=N_CH){
		fprintf(stderr, "ERROR in %s, channel should be between 0 & %d\n", __FUNCTION__, N_CH-1);
		return;
	}

	// nothing to do if not initialized
	if(c[ch].running==0) return;

	pthread_mutex_lock(&mtx[ch]);

	// signal to stop running and disable callback
	c[ch].running		= 0;

	// if helper thread is running, quit it
	if(c[ch].helper_enabled){
		// send signal to thread, this is just to make the blocking read quit
		if(en_debug) printf("sending SIGUSR1 to stop blocking reads in helper%d\n", ch);
		pthread_kill(c[ch].helper_thread_id, SIGUSR1);

		if(en_debug) printf("trying to join helper thread channel %d\n", ch);

#ifdef __ANDROID__
		errno = pthread_join(c[ch].helper_thread_id, NULL);
#else
		// do a timed join, 1 second timeout
		struct timespec thread_timeout;
		clock_gettime(CLOCK_REALTIME, &thread_timeout);
		thread_timeout.tv_sec += 1;
		errno = pthread_timedjoin_np(c[ch].helper_thread_id, NULL, &thread_timeout);
#endif
		if(errno==ETIMEDOUT){
			fprintf(stderr, "WARNING, %s timed out joining read thread\n", __FUNCTION__);
		}

		// thread joined, turn off the flag
		c[ch].helper_enabled = 0;
	}

	// remove the data pipe from the file system. This indicates to any other
	// clients that the data pipe name is now free to use
	if(c[ch].data_path[0]!=0){
		remove(c[ch].data_path);
	}

	// zero out the channel struct so we start fresh next time
	_clean_channel(ch);
	pthread_mutex_unlock(&mtx[ch]);

	return;
}


void pipe_client_close_all(void)
{
	for(int i=0; i<N_CH; i++) pipe_client_close_channel(i);
	return;
}


